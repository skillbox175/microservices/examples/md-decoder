package ru.skillbox.mdconverter.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ru.skillbox.mdconverter.dto.S3Event;
import ru.skillbox.mdconverter.service.S3EventsService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequiredArgsConstructor
public class S3HooksController {
    private final S3EventsService s3EventsService;

    @PostMapping(value = "s3/events", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> event(@RequestBody S3Event event) {
        log.info("S3 event received {}", event);
        s3EventsService.processEvent(event);
        log.info("S3 event processed");
        return ResponseEntity.accepted().build();
    }
}
