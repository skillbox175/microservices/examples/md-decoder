package ru.skillbox.mdconverter.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "aws.s3")
@Data
public class S3Properties {
    private String endpoint;
    private String accessKey;
    private String secretKey;
    private String region;
    private String bucketMd;
    private String bucketHtml;
    private String signer;
}
