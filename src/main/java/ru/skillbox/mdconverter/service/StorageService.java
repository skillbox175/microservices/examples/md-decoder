package ru.skillbox.mdconverter.service;

import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;
import lombok.RequiredArgsConstructor;
import ru.skillbox.mdconverter.repository.HtmlRepository;
import ru.skillbox.mdconverter.repository.MdRepository;
import ru.skillbox.mdconverter.repository.S3Repository;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Collection;
import java.util.Date;

@Service
@RequiredArgsConstructor
public class StorageService {
    private final MdRepository mdRepository;
    private final HtmlRepository htmlRepository;

    public Collection<String> getAllMdFiles() {
        return mdRepository.listKeys("/");
    }

    public InputStream getMdFileContent(String key) {
        return mdRepository.get(key)
                .map(S3Object::getObjectContent)
                .orElseThrow(() -> new IllegalStateException("Object not found " + key));
    }

    public LocalDateTime getMdFileModificationTime(String key) {
        return getObjectLastModified(mdRepository, key);
    }

    public LocalDateTime getHtmlFileModificationTime(String key) {
        return getObjectLastModified(htmlRepository, key);
    }

    public void putHtmlFile(String key, File file) throws IOException {
        try (var stream = new FileInputStream(file)) {
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentLength(file.length());
            metadata.setContentType(MediaType.TEXT_HTML_VALUE);
            htmlRepository.put(key, stream, metadata);
        }
    }

    public void deleteHtmlFile(String key) {
        htmlRepository.delete(key);
    }

    private LocalDateTime getObjectLastModified(S3Repository repository, String key) {
        return repository.get(key)
                .map(S3Object::getObjectMetadata)
                .map(ObjectMetadata::getLastModified)
                .map(this::dateToLocal)
                .orElse(LocalDateTime.MIN);
    }

    private LocalDateTime dateToLocal(Date date) {
        return LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
    }
}
