package ru.skillbox.mdconverter.service;

import lombok.RequiredArgsConstructor;
import org.apache.commons.io.IOUtils;
import ru.skillbox.mdconverter.properties.AppProperties;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

@Service
@RequiredArgsConstructor
public class ConvertingService {
    private final AppProperties appProperties;

    public void convertMdToHtml(InputStream md, OutputStream html) throws IOException {
        var pandocProcess = new ProcessBuilder(appProperties.getConvertParams()).start();
        try (var output = pandocProcess.getOutputStream()) {
            IOUtils.copy(md, output);
            output.flush();
        }
        try (var input = pandocProcess.getInputStream()) {
            IOUtils.copy(input, html);
            html.flush();
        }
    }
}
