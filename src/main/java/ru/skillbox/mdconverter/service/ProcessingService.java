package ru.skillbox.mdconverter.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.CompletableFuture;
import java.util.function.Supplier;

@Service
@RequiredArgsConstructor
@Slf4j
public class ProcessingService {
    private static final String EXT_MD = ".md";
    public static final String EXT_HTML = ".html";

    private final StorageService storageService;
    private final ConvertingService convertingService;

    @PostConstruct
    void init() {
        processMdFiles();
    }

    public void processMdFile(String file) {
        processMdFiles(Collections.singletonList(file));
    }

    public void processMdFiles() {
        processMdFiles(storageService.getAllMdFiles());
    }

    public void deleteLinkedHtmlFile(String object) {
        storageService.deleteHtmlFile(linkedHtmlFile(object));
    }

    private void processMdFiles(Collection<String> files) {
        var convertTasks = files.stream()
                .filter(this::isMdFile)
                .filter(this::isFileChanged)
                .map(this::processMdFileTask)
                .map(CompletableFuture::supplyAsync)
                .toList();
        CompletableFuture.allOf(convertTasks.toArray(new CompletableFuture[0])).join();
    }

    private boolean isFileChanged(String file) {
        var mdModified = storageService.getMdFileModificationTime(file);
        var htmlModified = storageService.getHtmlFileModificationTime(linkedHtmlFile(file));
        return mdModified.isAfter(htmlModified);
    }

    private Supplier<Boolean> processMdFileTask(String file) {
        return () -> {
            try {
                var tempFile = Files.createTempFile("temp", EXT_HTML);
                try (var md = storageService.getMdFileContent(file)) {
                    try (var output = new FileOutputStream(tempFile.toFile())) {
                        convertingService.convertMdToHtml(md, output);
                    }
                    storageService.putHtmlFile(linkedHtmlFile(file), tempFile.toFile());
                    return true;
                } finally {
                    Files.deleteIfExists(tempFile);
                }
            } catch (IOException e) {
                log.error("Can not convert md file {}: {}", file, e);
            }
            return false;
        };
    }

    private String linkedHtmlFile(String name) {
        return FilenameUtils.removeExtension(name) + EXT_HTML;
    }

    private boolean isMdFile(String file) {
        return file.toLowerCase().endsWith(EXT_MD);
    }

}
