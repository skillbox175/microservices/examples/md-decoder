package ru.skillbox.mdconverter.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ru.skillbox.mdconverter.dto.S3Event;
import ru.skillbox.mdconverter.properties.S3Properties;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class S3EventsService {
    private static final String EVENT_DELETE = "s3:ObjectRemoved:";
    private static final String EVENT_CREATE = "s3:ObjectCreated:";

    private final ProcessingService processingService;
    private final S3Properties properties;

    public void processEvent(S3Event event) {
        var bucket = extractBucketName(event.getKey());
        var object = extractObjectKey(event.getKey());
        var eventName = event.getEventName();
        if (bucket.equals(properties.getBucketMd()) && eventName != null) {
            if (eventName.startsWith(EVENT_CREATE)) {
                processingService.processMdFile(object);
                log.info("File updated {}", object);
            } else if (eventName.startsWith(EVENT_DELETE)) {
                processingService.deleteLinkedHtmlFile(object);
                log.info("File deleted {}", object);
            }
        }
    }

    private String extractBucketName(String key) {
        return key == null ? "" : key.substring(0, key.indexOf("/"));
    }

    private String extractObjectKey(String key) {
        return key == null ? "" : key.substring(key.indexOf("/"));
    }
}
