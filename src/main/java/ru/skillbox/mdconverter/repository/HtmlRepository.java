package ru.skillbox.mdconverter.repository;

import com.amazonaws.services.s3.AmazonS3;
import ru.skillbox.mdconverter.properties.S3Properties;
import org.springframework.stereotype.Component;

@Component
public class HtmlRepository extends S3Repository {
    public HtmlRepository(AmazonS3 s3Client, S3Properties properties) {
        super(s3Client, properties.getBucketHtml());
    }
}
