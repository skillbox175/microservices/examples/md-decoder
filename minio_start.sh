#!/bin/sh

docker run --rm -d --name minio \
        --publish 9000:9000 \
        --publish 9001:9001 \
        --env MINIO_ROOT_USER="minio_admin" \
        --env MINIO_ROOT_PASSWORD="minio_admin" \
        --volume miniodata:/data \
        bitnami/minio:latest